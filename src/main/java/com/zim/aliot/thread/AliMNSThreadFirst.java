package com.zim.aliot.thread;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.aliyun.mns.model.Message;
import com.zim.aliot.service.EntranceService;
import com.zim.aliot.service.ReceiverService;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class AliMNSThreadFirst extends Thread {
    @Autowired
    private ReceiverService receiverService;
    
    @Autowired
    private EntranceService entranceService;
    
    public AliMNSThreadFirst() {
        super.setName("AliMNSThreadFirst");
    }
    
    @Override
    public void run() {
        
        while (!this.isInterrupted()) {
            String body = null ;
            try {
                Message message = receiverService.receiveMessage();
                body = message.getMessageBodyAsString();
                entranceService.autoProcess(body);
            } catch (Exception e) {

                log.error("[AliMNSThreadFirstRun]:{}msg:{}",e.getMessage(),body);
            }
        }

        log.info("AliMNSThreadFirstRun循环完了");
    }

}
