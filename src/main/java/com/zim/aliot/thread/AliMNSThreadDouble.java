package com.zim.aliot.thread;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.aliyun.mns.model.Message;
import com.zim.aliot.service.EntranceService;
import com.zim.aliot.service.ReceiverService;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class AliMNSThreadDouble extends Thread{
    @Autowired
    private ReceiverService receiverService;
    
    @Autowired
    private EntranceService entranceService;
    
    public AliMNSThreadDouble() {
        super.setName("AliMNSThreadDouble");
    }
    
    @Override
    public void run() {
        
    	while (!this.isInterrupted()) {
            String body = null ;
            try {
                Message message = receiverService.receiveMessage();
                body = message.getMessageBodyAsString();
                entranceService.autoProcess(body);
            } catch (Exception e) {

                log.error("[AliMNSThreadDoubleRun]:{}msg:{}",e.getMessage(),body);
            }
        }

        System.out.println("AliMNSThreadDouble循环完了");
    }
}
