package com.zim.aliot.thread;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.aliyun.mns.model.Message;
import com.zim.aliot.service.EntranceService;
import com.zim.aliot.service.ReceiverService;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class AliMNSThreadThree extends Thread{
    @Autowired
    private ReceiverService receiverService;
    
    @Autowired
    private EntranceService entranceService;
    
    public AliMNSThreadThree() {
        super.setName("AliMNSThreadThree");
    }
    
    @Override
    public void run() {
        
    	 while (!this.isInterrupted()) {
             String body = null ;
             try {
                 Message message = receiverService.receiveMessage();
                 body = message.getMessageBodyAsString();
                 entranceService.autoProcess(body);
             } catch (Exception e) {

                 log.error("[AliMNSThreadThreeRun]:{}msg:{}",e.getMessage(),body);
             }
         }

        System.out.println("AliMNSThreadThree循环完了");
    }
}
