package com.zim.aliot.lcx;

import java.io.Serializable;

import lombok.Data;

/**
 * 事件通知
 * @author zi-m.cn
 */
@Data
public class LcxEvent  implements Serializable{

    private static final long serialVersionUID = 2845953800047722860L;

    /**
     * 车架号
     */
    private String car_unique_id;
    
    /**
     * 事件
     */
    private String event;
    
    /**
     * 动作
     */
    private int action;
    
    /**
     * 事件发生时间
     */
    private int event_time;
    
    /**
     * 事件推送时间
     */
    private int send_time;
}
