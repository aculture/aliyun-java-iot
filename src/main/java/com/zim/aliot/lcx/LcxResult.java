package com.zim.aliot.lcx;

import lombok.Data;

/**
 * Lcx接口返回
 * @author zi-m.cn
 */
@Data
public class LcxResult {
    
    private int result;
    
    private String msg;
    
    private Object data;
}
