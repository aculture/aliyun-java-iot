package com.zim.aliot.util;

import java.io.UnsupportedEncodingException;

import org.springframework.util.DigestUtils;

public class MD5Utils {
    public static String decode(String string) {
        try {
            return DigestUtils.md5DigestAsHex(string.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            
        }
        
        return null;
    }
}
