package com.zim.aliot.util;

import com.zim.aliot.vo.ResultVO;

public class ResultVOUtils {
    public static ResultVO<Object> success(Object object) {
        ResultVO<Object> resultVO = new ResultVO<Object>();
        resultVO.setData(object);
        resultVO.setCode(0);
        resultVO.setMsg("成功");
        return resultVO;
    }

    public static ResultVO<Object> success() {
        return success(null);
    }

    public static ResultVO<Object> error(Integer code, String msg) {
        ResultVO<Object> resultVO = new ResultVO<Object>();
        resultVO.setCode(code);
        resultVO.setMsg(msg);
        return resultVO;
    }
}
