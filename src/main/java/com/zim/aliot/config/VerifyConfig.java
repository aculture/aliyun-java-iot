package com.zim.aliot.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
@ConfigurationProperties(prefix = "verify")
public class VerifyConfig {
	
	public String userName;
	
	public String key;
}
