package com.zim.aliot.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
@ConfigurationProperties(prefix = "saika")
public class SaiKaConfig {
	
	private String ftp;
}
