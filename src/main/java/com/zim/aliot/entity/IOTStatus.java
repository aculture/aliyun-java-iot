package com.zim.aliot.entity;

import lombok.Data;

@Data
public class IOTStatus {
    /**
     * 设备状态
     */
    private String status;
    
    private String productKey;
    
    private String deviceName;
    
    /**
     * 发送通知时间点
     */
    private String time;
    
    /**
     * 发送通知UTC时间点
     */
    private String utcTime;
    
    /**
     * 状态变更时最后一次通信时间
     */
    private String lastTime;
    
    /**
     * 状态变更时最后一次通信UTC时间
     */
    private String utcLastTime;
    
    /**
     * 设备端公网出口IP
     */
    private String clientIp;
    
}
