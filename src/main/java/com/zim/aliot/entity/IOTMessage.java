package com.zim.aliot.entity;

import lombok.Data;
/**
 * iot message entity
 * @author zi-m.cn
 * @date 2018/10/19
 */
@Data
public class IOTMessage {
    /**
     * 设备上传内容
     */
    String payload;
    
    /**
     * 消息类型
     */
    String messagetype;
    
    /**
     * 主题
     */
    String topic;
    
    /**
     * 消息ID
     */
    String messageid;
    
    /**
     * 消息时间
     */
    int timestamp;
}
