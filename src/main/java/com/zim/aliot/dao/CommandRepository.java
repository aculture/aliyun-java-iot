package com.zim.aliot.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.zim.aliot.dataobject.CommandDo;

@Repository
public interface CommandRepository extends MongoRepository<CommandDo,String>{

}
