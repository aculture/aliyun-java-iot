package com.zim.aliot.form;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class TakePhotoForm {
	@NotEmpty(message = "车架号不能为空")
	private String car_unique_id;
	
	@NotEmpty(message = "请求唯一标识不能为空")
	private String seq_id;
	
	@NotNull(message = "下发命令时的时间戳不能为空")
	private long time;
}
