package com.zim.aliot.dataobject;

import java.util.Date;

import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "RequestCommand")
@CompoundIndexes({
        @CompoundIndex(name = "carUniqueId_command_requestSn_push_idx", def = "{'carUniqueId': -1, 'command': -1, 'requestSn': -1 ,'push':-1}")
})
public class RequestCommand {


    @Indexed(name = "push", direction = IndexDirection.DESCENDING)
    @Field
    private int push = 0;  //0：待返回 ；1可推送 ；2已推送

    // 还可以生成索引
    @Indexed(name = "carUniqueId", direction = IndexDirection.DESCENDING)
    @Field
    private String carUniqueId;

    @Field
    private String machineNum;

    @Field
    private String seqId;

    @Field
    private String command;

    @Field
    private int action;

    @Field
    private long requestTime;

    @Field
    private int requestSn;

    @Field
    private long responseTime;

    @Field
    private int commandResult = -1;  //命令执行状态，-1执行中;0执行成功;1执行失败

    @Field
    private String pwd = "";

    @Field
    private String longitude = "";

    @Field
    private String latitude = "";

    @Field
    private String photo = "";

    @Field
    private String version = "";

    @Field
    private int rentType = -1;

    @Indexed(name = "createTime", direction = IndexDirection.DESCENDING,expireAfterSeconds = 3600)
    @Field
    private Date createTime;

    public int getPush() {
        return push;
    }

    public void setPush(int push) {
        this.push = push;
    }

    public String getCarUniqueId() {
        return carUniqueId;
    }

    public void setCarUniqueId(String carUniqueId) {
        this.carUniqueId = carUniqueId;
    }

    public String getMachineNum() {
        return machineNum;
    }

    public void setMachineNum(String machineNum) {
        this.machineNum = machineNum;
    }

    public String getSeqId() {
        return seqId;
    }

    public void setSeqId(String seqId) {
        this.seqId = seqId;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }

    public long getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(long requestTime) {
        this.requestTime = requestTime;
    }

    public int getRequestSn() {
        return requestSn;
    }

    public void setRequestSn(int requestSn) {
        this.requestSn = requestSn;
    }

    public long getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(long responseTime) {
        this.responseTime = responseTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public int getCommandResult() {
        return commandResult;
    }

    public void setCommandResult(int commandResult) {
        this.commandResult = commandResult;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public int getRentType() {
        return rentType;
    }

    public void setRentType(int rentType) {
        this.rentType = rentType;
    }
}