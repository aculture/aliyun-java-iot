package com.zim.aliot.dataobject;

import java.io.Serializable;

import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "joy_command")
public class CommandDo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2266752090052185155L;


	/**
	 * 车辆控制
	 */

	@Indexed(name = "vin", direction = IndexDirection.DESCENDING)
	private String vin;
	
	private String seqId;
	
	private String command;
	
	private int action;
	
	@Indexed(name = "time", direction = IndexDirection.DESCENDING)
	private long time;
	
	private int status;  // 0  可推送   1推送成功   2 推送失败
	
	private int type;
	
	private String version;
	
	private String password;
	
	/**
	 * 流水
	 */
	private int serialNumber;

}
