package com.zim.aliot.service;

import com.google.gson.JsonSyntaxException;
import com.zim.aliot.entity.IOTMessage;

public interface EntranceService {
    /**
     * 解析IOT上传消息
     * @param message
     * @return
     * @throws JsonSyntaxException
     */
    IOTMessage getMnsMessage(String message) throws JsonSyntaxException;
    
    /**
     * 分离设备状态与数据
     * @param message
     * @throws Exception
     * @throws JsonSyntaxException
     */
    void autoProcess(String message) throws Exception;
}
