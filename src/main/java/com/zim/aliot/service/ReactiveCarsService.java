package com.zim.aliot.service;

import com.zim.aliot.dataobject.CarsDo;


import reactor.core.publisher.Mono;

public interface ReactiveCarsService {
    
    Mono<CarsDo> save(CarsDo joyCarsPO);

}
