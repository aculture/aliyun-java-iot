package com.zim.aliot.service;

public interface AliClientService {
    
    /**
     * 发送byte消息
     * @param message
     * @param deviceName
     * @return boolean
     */
    boolean pubMessage(byte[] message,String deviceName);
    
    /**
     * 设备注册
     * @param deviceName
     * @return 
     */
    String registDevice(String deviceName);
}
