package com.zim.aliot.service;

import java.util.List;

import com.zim.aliot.dataobject.CarsDo;
import com.zim.aliot.vo.VehicleVO;

public interface CarsService {
    
    /**
     * 保存cars表数据
     * @param carsDo
     * @return
     */
    CarsDo save(CarsDo carsDo);
    
    /**
     * @throws Exception 
     * 
     */
    CarsDo find(String deviceName);
    
     
    List<VehicleVO> getHisInfo(String vin,long timeStart,long timeEnd);
    
    CarsDo findByVin(String vin);
    
    boolean bandCars(int supplierNo,String equipMentId,String vin,String carSn);
    
    boolean unBandCar(int supplierNo,String vin);
}
