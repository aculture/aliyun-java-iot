package com.zim.aliot.service;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@Service
public class CurlService {
    private final RestTemplate restTemplate;
    
    public CurlService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }
    
    public RestTemplate getRestTemplate() {
        return restTemplate;
    }
    public String someRestCall(String name) throws RestClientException{
        return this.restTemplate.getForObject("http://centos7.com?aaa={1}", String.class, name);
    }
    
    public <T>ResponseEntity<T> post(@Nullable Object request,Class<T> responseType) {
        return this.restTemplate.postForEntity("http://centos7.com", request, responseType);
    }
    
    /**
     * http json header
     * @param MultiValueMap<java.lang.String,java.lang.String> values
     * @return HttpHeaders
     */
    public HttpHeaders jsonHeaders(MultiValueMap<String, String> values) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        
        headers.addAll(values);
        
        return headers;
    }
    
    /**
     * http json header
     * @return HttpHeaders
     */
    public HttpHeaders jsonHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }
}
