package com.zim.aliot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aliyun.mns.client.CloudQueue;
import com.aliyun.mns.model.Message;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ReceiverService {
    public static final int WAIT_SECONDS = 30;
    
    @Autowired
    private CloudQueue cloudQueue;
    
    private String lockObj = "a1idNzoUyCt";
    
    private static boolean sPollingMap = false;
    
    public boolean setPolling()
    {
        synchronized (lockObj) {
            Boolean ret = sPollingMap;
            if (ret == null || !ret) {
                sPollingMap = true;
                return true;
            }
            return false;
        }
    }
    
    public void clearPolling()
    {
        synchronized (lockObj) {
            sPollingMap = false;
            lockObj.notifyAll();
            log.info("Everyone WakeUp and Work!");
        }
    }
    
    public Message receiveMessage()
    {    
        boolean polling = false;
        while (true) {
            synchronized (lockObj) {
                Boolean p = sPollingMap;
                if (p != null && p) {
                    try {
                        log.info("Thread Have a nice sleep! :{}",Thread.currentThread().getName());
                        polling = false;
                        lockObj.wait();
                    } catch (InterruptedException e) {
                        log.error("MessageReceiver Interrupted! QueueName is:{}",e);
                        return null;
                    }
                }
            }
            try {
                Message message = null;
                if (!polling) {
                    message = cloudQueue.popMessage();
                    if (message == null) {
                        //log.info("取到空值,跳过本次循环");
                        polling = true;
                        continue;
                    }
                }else {
                    if (setPolling()) {
                        log.info("Thread Polling! :{}",Thread.currentThread().getName());
                    } else {
                        continue;
                    }
                    do {
                        log.info("Thread KEEP Polling! :{}",Thread.currentThread().getName());
                        try {
                            message = cloudQueue.popMessage(WAIT_SECONDS);
                        } catch(Exception e)
                        {
                            log.error("Exception Happened when polling popMessage: {}",e);
                        }
                    } while (message == null);
                    clearPolling();
                }
                if(message!=null) {
                    cloudQueue.deleteMessage(message.getReceiptHandle());
//                    log.info("delete message successfully.!");
                }
                return message;
            } catch (Exception e) {
                log.error("Exception Happened when popMessage:{}",e);
            }
        }
    }
    
}
