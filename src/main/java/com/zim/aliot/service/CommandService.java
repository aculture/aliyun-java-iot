package com.zim.aliot.service;

import com.zim.aliot.dataobject.CommandDo;

public interface CommandService {
	boolean commandAction(String vin,String seqId,String command,int action,long time,int type,String version,String pwd);
	
	CommandDo save(CommandDo commandDo);
	
	CommandDo find(String vin,String command,int serialNumber);
	
	void updateStatus(String vin,String command,int serialNumber,int status);
	
	

}
