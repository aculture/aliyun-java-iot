package com.zim.aliot.service;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public interface RestService {
    
    RestTemplate getRestTemplate();
    
    <T> ResponseEntity<T> requestLai(String url,Class<T> request,ParameterizedTypeReference<T> responseType);
    
    
}
