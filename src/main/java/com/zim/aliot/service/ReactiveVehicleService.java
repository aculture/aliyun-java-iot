package com.zim.aliot.service;

import com.zim.aliot.vo.VehicleVO;

public interface ReactiveVehicleService {

     void save(VehicleVO vehicleVO);
}
