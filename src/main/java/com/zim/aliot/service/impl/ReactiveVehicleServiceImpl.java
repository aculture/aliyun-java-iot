package com.zim.aliot.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.util.concurrent.RateLimiter;
import com.zim.aliot.dao.ReactiveVehicleRepository;
import com.zim.aliot.service.ReactiveVehicleService;
import com.zim.aliot.vo.VehicleVO;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Service
@Slf4j
public class ReactiveVehicleServiceImpl implements ReactiveVehicleService {
	
	private final RateLimiter rateLimiter = RateLimiter.create(0.1);

    @Autowired
    private ReactiveVehicleRepository reactiveVehicleRepository;
    
    @Override
    public void save(VehicleVO vehicleVO) {
//        log.info("要保存的数据:{}",vehicleVO);
    	Mono<VehicleVO> veMono = reactiveVehicleRepository.save(vehicleVO);
    	
    	veMono.subscribe(res->{
    		if (rateLimiter.tryAcquire()) {
    			
    			log.info("[GPS保存的数据]");
    		}
    	});
       
    }

}
