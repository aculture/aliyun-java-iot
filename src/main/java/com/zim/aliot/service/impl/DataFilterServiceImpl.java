package com.zim.aliot.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zim.aliot.async.NotifyCommand;
import com.zim.aliot.service.DataFilterService;
import com.zim.aliot.vo.VehicleVO;
import com.zim.analysis.po.GpsPO;

@Service
public class DataFilterServiceImpl implements DataFilterService {
	@Autowired
    private NotifyCommand notifyCommand;

    @Override
    public VehicleVO filterHandle(GpsPO gpsPO, VehicleVO oldVehicleVO) {
        VehicleVO vehicleVO = new VehicleVO();

        vehicleVO.setNetwork_status(1);
        vehicleVO.setCharge_status(gpsPO.getChargeStatus());
        vehicleVO.setSpeed(gpsPO.getSpeed());
        vehicleVO.setPower_model(1);

        if (oldVehicleVO == null) {

            vehicleVO.setLongitude(gpsPO.getLongitude());
            vehicleVO.setLatitude(gpsPO.getLatitude());
            vehicleVO.setDirection(gpsPO.getDiretion());
            vehicleVO.setSoc(gpsPO.getSoc() / 10);
            vehicleVO.setMileage_surplus(gpsPO.getEndurance() / 10);
            vehicleVO.setTotal_electricity(gpsPO.getTotalElecticity() / 10);
            vehicleVO.setTotal_voltage(gpsPO.getTotalVolatge() / 10);
            vehicleVO.setMileage(gpsPO.getMileage() / 10);
            

        } else {
            if (gpsPO.getPositioning() == 1) {
                vehicleVO.setLongitude(gpsPO.getLongitude());
                vehicleVO.setLatitude(gpsPO.getLatitude());
                vehicleVO.setDirection(gpsPO.getDiretion());
                vehicleVO.setGps_speed(gpsPO.getGpsSpeed());

            } else {
                vehicleVO.setLongitude(oldVehicleVO.getLongitude());
                vehicleVO.setLatitude(oldVehicleVO.getLatitude());
                vehicleVO.setDirection(oldVehicleVO.getDirection());
                vehicleVO.setGps_speed(oldVehicleVO.getGps_speed());
            }

            if (gpsPO.getSoc() > 0 && gpsPO.getSoc() < 1000) {
                if (gpsPO.getReady() == 1 || gpsPO.getChargeStatus() == 1 || oldVehicleVO.getSoc() == 0) {
                    vehicleVO.setSoc(gpsPO.getSoc() / 10);
                } else {
                    vehicleVO.setSoc(oldVehicleVO.getSoc());
                }
            } else {
                vehicleVO.setSoc(oldVehicleVO.getSoc());
            }

            if (gpsPO.getTotalVolatge() > 0 && gpsPO.getTotalVolatge() < 10000) {
                // 总电压（单位：v）
                vehicleVO.setTotal_voltage(gpsPO.getTotalVolatge() * 0.1);
            } else {
                // 总电压（单位：v）
                vehicleVO.setTotal_voltage(oldVehicleVO.getTotal_voltage());
            }

            	
            if( gpsPO.getEndurance()>1 && gpsPO.getEndurance()==1 || gpsPO.getChargeStatus()==1) {
        		//剩余里程（单位km）
            	vehicleVO.setMileage_surplus(gpsPO.getEndurance() / 10);
        	}else {
        		//剩余里程（单位km）
        		vehicleVO.setMileage_surplus(oldVehicleVO.getMileage_surplus());
			}
            
            if (gpsPO.getMileage() > 0 && gpsPO.getMileage() < 9999999) {
                // 里程（单位：km）
                vehicleVO.setMileage(gpsPO.getMileage() * 0.1);
            } else {
                // 里程（单位：km）
                vehicleVO.setMileage(oldVehicleVO.getMileage());
            }
            
          
            
            
            //点熄火事件
            if( gpsPO.getReady() != oldVehicleVO.getReady_status() ) {
            	notifyCommand.eventSend(gpsPO, "IGNITE", gpsPO.getReady() == 0 ?"0":"1");
            }
            
            //门状态 加 后备箱
            if( gpsPO.getDoor() != oldVehicleVO.getDoor_lb()) {
                notifyCommand.eventSend(gpsPO, "DOOR", gpsPO.getDoor() == 0?"0":"1");
                notifyCommand.eventSend(gpsPO, "TRUNK", gpsPO.getDoor() == 0?"0":"1");
            }
            
            //OBD
            if( gpsPO.getObdStatus() != oldVehicleVO.getObd_status()) {
            
                notifyCommand.waringEvent(gpsPO);
            }
            
            //行车灯
            if( gpsPO.getRunLight() != oldVehicleVO.getRunning_lights()) {
                notifyCommand.eventSend(gpsPO, "LIGHTS", gpsPO.getRunLight() ==0?"0":"1");
            }
            
            
        }
        
        vehicleVO.setLaunch_status(gpsPO.getReady());
        vehicleVO.setDoor_lb(gpsPO.getDoor());
        vehicleVO.setDoor_lf(gpsPO.getDoor());
        vehicleVO.setDoor_rb(gpsPO.getDoor());
        vehicleVO.setDoor_rf(gpsPO.getDoor());
        vehicleVO.setTrunk(gpsPO.getDoor());
        vehicleVO.setVoltage(gpsPO.getBatteryVoltage());
        vehicleVO.setGear(gpsPO.getGear());
        vehicleVO.setObd_status(gpsPO.getObdStatus());
        vehicleVO.setMachine_version(gpsPO.getVersion());
        vehicleVO.setGprs_signal(gpsPO.getGprsSignal());
        vehicleVO.setGps_signal(gpsPO.getSatellite());
        vehicleVO.setPositioning_validity(gpsPO.getPositioning());
        vehicleVO.setTime(gpsPO.getTime());
        vehicleVO.setMachine_report_time(gpsPO.getTime());
        vehicleVO.setMachine_platform_receive_time(System.currentTimeMillis());
        vehicleVO.setAcc_status(gpsPO.getAccStatus());
        vehicleVO.setReady_status(gpsPO.getReady());
        vehicleVO.setMind_cons(gpsPO.getCenterLock());
        vehicleVO.setAlarm(gpsPO.getAlarm());
        vehicleVO.setRunning_lights(gpsPO.getRunLight());
        vehicleVO.setReturn_car_status(gpsPO.getReturnCar());
        vehicleVO.setLong_term_lease(gpsPO.getLongRenting());
        vehicleVO.setControl_lock(gpsPO.getCtrlLock());
        
        return vehicleVO;

    }

}
