package com.zim.aliot.exception;

import com.zim.aliot.enums.ResultEnum;

public class IotApiException extends RuntimeException {


    /**
     * 自定义异常类
     */
    private static final long serialVersionUID = 3545368830860813127L;
    private int code;
    
    public IotApiException(ResultEnum resultEnum) {
        super(resultEnum.getMessage());

        this.setCode(resultEnum.getCode());
    }
    
    public IotApiException(int code, String message) {
        super(message);
        this.setCode(code);
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
