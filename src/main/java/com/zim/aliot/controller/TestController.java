package com.zim.aliot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zim.aliot.async.NotifyCommand;
import com.zim.analysis.po.GpsPO;

@RestController

public class TestController {
	
    @Autowired
    private NotifyCommand notifyCommand;
    
    
    
    @GetMapping("ackNotify")
    public void ackNotify(@RequestParam(name = "command",defaultValue="",required=true) String command,
    		@RequestParam(name = "serialNumber",defaultValue="",required=true) int serialNumber) {
    	
		notifyCommand.ackNotify("LNBSCB3F0000001", command, serialNumber, true,"{sacfasc}");
    }
    
    @GetMapping("eventSend")
    public void eventSend(@RequestParam(name = "command",defaultValue="",required=true) String command,
    		@RequestParam(name = "action",defaultValue="",required=true) String action) {
    	GpsPO gpsPO= new GpsPO();
    	gpsPO.setDeviceName("0000000B121000000002");
    	gpsPO.setTime(1552369714);
		notifyCommand.eventSend(gpsPO, command, action);
    }
    
    @GetMapping("waringEvent")
    public void waringEvent() {
    	GpsPO gpsPO= new GpsPO();
    	gpsPO.setDeviceName("0000000B121000000002");
    	gpsPO.setTime(1552369714);
    	gpsPO.setObdStatus(0);
		notifyCommand.waringEvent(gpsPO);
    }
}
