package com.zim.aliot.controller;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zim.aliot.client.LcxClient;
import com.zim.aliot.dao.ReactiveCarsRepository;
import com.zim.aliot.dataobject.CarsDo;
import com.zim.aliot.lcx.LcxEvent;
import com.zim.aliot.lcx.LcxResult;

import reactor.core.publisher.Mono;

@RestController
@RequestMapping(value = "/city")
public class CityWebFluxController {

    @Autowired
    private ReactiveCarsRepository reactiveCarsRepository;

    @Autowired
    private LcxClient lcxClient;

    @Resource
    private RedisTemplate<String, LcxEvent> redisTemplate;

    @GetMapping(value = "list")
    public void redis() {
        LcxEvent lcxEvent = new LcxEvent();
        lcxEvent.setCar_unique_id("LNBSCB3F0HW216924");
        lcxEvent.setAction(1);
        lcxEvent.setEvent(String.valueOf(System.currentTimeMillis()));
        lcxEvent.setEvent_time(123456);
        lcxEvent.setSend_time(555562);
        ListOperations<String, LcxEvent> list = redisTemplate.opsForList();
        list.rightPush("event_list", lcxEvent);
    }

    @GetMapping(value = "pop")
    public LcxEvent pop() {
        ListOperations<String, LcxEvent> list = redisTemplate.opsForList();
        return list.leftPop("event_list");
    }

    @GetMapping(value = "/")
    public Mono<CarsDo> findCityById() {
        return reactiveCarsRepository.findById("222");
    }

    @GetMapping(value = "/curl")
    public void curl() {
        LcxEvent lcxEvent = new LcxEvent();
        lcxEvent.setCar_unique_id("LNBSCB3F0HW216924");
        lcxEvent.setAction(1);
        lcxEvent.setEvent("DIANHUO");
        lcxEvent.setEvent_time(123456);
        lcxEvent.setSend_time(555562);

        MultiValueMap<String, String> obdStatus = new LinkedMultiValueMap<String, String>();
        obdStatus.add("car_unique_id", "LNBSCB3F0HW216924");
        obdStatus.add("event", "DIANHUO");
        obdStatus.add("action", "1");
        obdStatus.add("event_time", "123456");
        obdStatus.add("send_time", "5555");

        // String result = lcxClient.request("send_car_event", lcxEvent);
        // String result = lcxClient.request("send_car_event", obdStatus);
        LcxResult result = lcxClient.sendEvent("LNBSCB3F0HW216924", "DIANHUO", "1", 123456, 5555);
        if (null != result) {
            System.out.println(result);
        }
        // ParameterizedTypeReference<ResultVO<Book>> responseType = new
        // ParameterizedTypeReference<ResultVO<Book>>() {};
        // lcxClient.request("http://centos7.com", request);
    }

}
