package com.zim.aliot.vo;

import java.io.Serializable;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "joy_vehicle")
public class VehicleVO implements Serializable {
    
    /**
     * 整车数据
     */
    private static final long serialVersionUID = 5409189702875181715L;
    
    @Indexed(name = "car_unique_id", direction = IndexDirection.DESCENDING)
    private String car_unique_id;
    /**
     * 车机联网状态,0：离线；1：在线
     */
    private int network_status;
    /**
     * 充放电状态,0：未充电；1：充电
     */
    private int charge_status;
    /**
     * 经度
     */
    private double longitude;
    /**
     * 纬度
     */
    private double latitude;
    
    /**
     * GPS速度 km/h (使用GSM经纬度时表示精确度的低8位)
     */
    private double gps_speed;
    
    /**
     * 速度
     */
    private double speed;
    /**
     * 方向，有效值范围：0～359，正北为0，顺时针。（单位：度）
     */
    private int direction;
    /**
     * 运行动力模式 ，1：纯电；2：混动；3：燃油；4：表示异常
     */
    private int power_model;
    /**
     * 里程（单位：km）
     */
    private double mileage;
    /**
     * 总电压（单位：v）
     */
    private double total_voltage;
    /**
     * 总电流（单位：A）
     */
    private double total_electricity;
    /**
     * 电量百分比，0～100（表示0%～100%），最小计量单元：1% 单位（%）
     */
    private int soc;
    /**
     * 点熄火状态，0熄火状态，1点火状态
     */
    private int launch_status;
    /**
     * 剩余里程（单位km）
     */
    private double mileage_surplus;
    /**
     * 左前门开关状态，0关门状态，1开门状态
     */
    private int door_lf;
    /**
     * 右前门开关状态，0关门状态，1开门状态
     */
    private int door_rf;
    /**
     * 左后门开关状态，0关门状态，1开门状态
     */
    private int door_lb;
    /**
     * 右后门开关状态，0关门状态，1开门状态
     */
    private int door_rb;
    /**
     * 后备箱开关状态，0关门状态，1开门状态
     */
    private int trunk;
    /**
     * 小电瓶电压
     */
    private double voltage;
    /**
     * 挡位：R:R挡位；N:N挡位；D:D挡位
     */
    private String gear;
    /**
     * obd拔插状态，0拔掉，1插上
     */
    private int obd_status;
    /**
     * 车机版本号
     */
    private String machine_version;
    /**
     * gprs信号强度
     */
    private int gprs_signal;
    /**
     * gps信号强度
     */
    private int gps_signal;
    /**
     * 定位数据有效性,0无效；1有效
     */
    private int positioning_validity;
    /**
     * 该定位数据的时间戳（单位：秒）
     */
    @Indexed(name = "time", direction = IndexDirection.DESCENDING)
    private long time;
    /**
     * 车机上报该定位数据的时间戳（单位：毫秒）
     */
    private long machine_report_time;
    /**
     * 车机平台接收该定位数据的时间戳（单位：毫秒）
     */
    private long machine_platform_receive_time;
    
    /**
     * 中控锁状态，1为锁 0为未锁
     */
    private int mind_cons;
    
    /**
     * 防盗报警状态 1:报警; 0:正常
     */
    private int alarm;
    
    /**
     * acc（钥匙）状态，0：off状态；1：on状态；
     */
    private int acc_status;
    /**
     * ready状态，0：非ready状态；1：ready状态；
     */
    private int ready_status;
    
    
    private int return_car_status ;
    
    private int long_term_lease;
    
    /**
     * 行车灯/示宽灯，1为开，0为关
     */
    private int running_lights;
    
    /**
     * 控车锁状态 1为锁定 0 为未锁定
     */
    private int control_lock;
}
