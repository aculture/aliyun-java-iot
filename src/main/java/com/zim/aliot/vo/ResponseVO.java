package com.zim.aliot.vo;

import lombok.Data;

@Data
public class ResponseVO {
    /** 错误码. */
    private Integer code;

    /** 提示信息. */
    private String msg;

    /** 具体内容. */
    private Book data;
}
