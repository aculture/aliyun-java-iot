package com.zim.aliot.vo;

import lombok.Data;

@Data
public class Book {
    private String name;
    
    private int price;
    
    private String author;

}
